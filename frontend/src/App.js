import logo from './logo.svg';
import React, { Component } from 'react';
import SimpleMap from './components/SimpleMap'
import Api from './components/api.js'
import './App.css';


class App extends Component{

  API = new Api()
  state = {
    loadMap: false,
    data: []
  }
  constructor(props){
    super(props)
    this.locationsHandler = this.locationsHandler.bind(this)
  }
  async componentDidMount(){
      await this.API.getLocationData(this.locationsHandler)
  }

  async locationsHandler(locationData){
    console.log("Entered handler")
    await this.setState({
      data: locationData
    })
  }
  render(){
    console.log(this.state.data)
    return (
      <div className="App">
        <h1>Test Header</h1>
        <SimpleMap center={{ lat:30.41965495487671, lng: -97.72842360495181 }} zoom={14} positions={this.state.data}/>
      </div>
    );
  }
}

export default App;
