import React, { Component } from 'react';
import {Map, HeatMap, GoogleApiWrapper} from 'google-maps-react';
import './map.css'

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const gradient = [
    "rgba(0, 255, 255, 0)",
    "rgba(0, 255, 255, 1)",
    "rgba(0, 191, 255, 1)",
    "rgba(0, 127, 255, 1)",
    "rgba(0, 63, 255, 1)",
    "rgba(0, 0, 255, 1)",
    "rgba(0, 0, 223, 1)",
    "rgba(0, 0, 191, 1)",
    "rgba(0, 0, 159, 1)",
    "rgba(0, 0, 127, 1)",
    "rgba(63, 0, 91, 1)",
    "rgba(127, 0, 63, 1)",
    "rgba(191, 0, 31, 1)",
    "rgba(255, 0, 0, 1)"
  ];

class SimpleMap extends Component {
  static defaultProps = {
    center: {
        lat: 59.95,
        lng: 30.33
    },
    zoom: 1
  };

  render() {
    return (
      // Important! Always set the container height explicitly

      <div className = "mapDiv">
          <h1 className="test">CSS TEST</h1>
        <Map id = "mapBoi"
        //   style={{height: '200px', width: '200px'}}
          google={this.props.google}
          className="mapBoi"
          zoom={this.props.zoom}
          initialCenter={this.props.center}
          onReady={this.handleMapReady}
        >

        <HeatMap
            gradient={gradient}
            positions={this.props.positions}
            opacity={.5}
            radius={20}
          />
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyBEjHBaa-Pd--26vOhS1YzAlFH5KvDzTBg",
    libraries: ["visualization"]
  })(SimpleMap);