class Api{

    constructor(){

    }

    print(){
        console.log("hitting the APi")
    }

    async getLocationData(handler){
        let url = "http://127.0.0.1:8000/locations/"
        fetch(url).then(response => {
            response.json().then(data =>{
                handler(data)
            })
        })
    }

}

export default Api;