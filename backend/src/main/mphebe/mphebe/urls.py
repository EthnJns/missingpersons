
from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('health/', views.health, name='health'),
    path('locations/', views.locations, name='locations')
]
