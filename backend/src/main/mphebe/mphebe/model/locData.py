import json
import os
class Location:
    lat = ''
    lng = ''
    def __init__(self, latitude, longitude):
        self.lat = latitude
        self.lng = longitude
    
    def __str__(self):
        return str(self.lat) + ', ' + str(self.lng)

    def toJSON(self):
        return json.dumps(self, default=lambda o:o.__dict__, sort_keys=True, indent=4)


class LocationData:
    locations = []

    def __init__(self):
        self.locations = []
        filePath = str(self.getFilePath())+"/mphebe/resources/locations.txt"
        location_data_file = open(filePath, 'r')
        location_data = location_data_file.read()
        locationsSplit = location_data.split(',')
        for lat_lng in locationsSplit:
            loc = lat_lng.split(':')
            self.locations.append(Location(loc[0], loc[1]))

    def add_location(self, location):
        self.locations.append(location)

    def toJSON(self):
        jsonString = '['
        for t in self.locations:
            jsonString += t.toJSON() + ","
        jsonString = jsonString[:-1]+']'
        
        return jsonString

    def getFilePath(self):
        currentDirectory = os.getcwd()
        return currentDirectory


l = LocationData()