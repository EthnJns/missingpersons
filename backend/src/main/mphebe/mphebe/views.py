from django.http import HttpResponse
from .model.locData import LocationData
from .model.locData import Location


def health(request):
    return HttpResponse("Site is currently up")


def locations(request):
    ld = LocationData()
    jsonData = ld.toJSON()
    print(len(jsonData))
    del ld
    return HttpResponse(jsonData)