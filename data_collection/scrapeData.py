from scraper import Scraper
from date_inc import OffenseDate
import time

base_url = "https://www.austintexas.gov/police/reports/search2.cfm?startdate=10%2F01%2F2021&numdays=6&address=&rucrext=4103%3A0&tract_num=&zipcode=&zone=&district=&city=&choice=criteria&Submit=Submit"

address_file = open('addresses1.txt', 'w+')
s = Scraper()
OD = OffenseDate('04/05/20', '10/05/21', 6)
driver = s.get_driver(base_url)
s.acknowledge(driver)


while OD.passed_end_date() == False:
    #We need to navigate to the advance search page
    s.advance_search(driver)
    date_field = s.get_date_input(driver)
    day_inc_field = s.get_day_input(driver)
    offense_selector = s.get_dropdown_offense(driver)
    s.select_offense(offense_selector, s.KIDNAPPING)
    s.enter(date_field, OD.current_date, False)
    s.enter(day_inc_field, "6", True)
    day_inc_field.click()
    time.sleep(.5)
    s.submit(driver)
    time.sleep(.5)
    s.get_table_data(driver)
    s.new_search(driver)
    OD.increment_date()

address_file.writelines(s.location_set)
address_file.close()
