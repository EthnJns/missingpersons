import requests
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


class Scraper:
    START_DATE = "inputStartDate"
    NUMBER_ADD_DAYS =  "inputAddDays"
    SUBMIT = 'Submit'
    KIDNAPPING = 262
    MISSING_PERSON = 281
    start_date = "11/01/20"
    offense_location = "Offense Location"
    location_set = []

    def __init__(self):
        self.location_set = []

    def advance_search(self, driver):
        adv = driver.find_elements_by_tag_name('p')
        adv_button = adv[2]
        adv_button.click()
        time.sleep(.5)

    def acknowledge(self, driver):
        ack = driver.find_element_by_name('agreement')
        ack.click()
        time.sleep(.5)
        

    def get_driver(self, url):
        driver = webdriver.Firefox()
        driver.get(url)
        time.sleep(.5)
        return driver

    def get_date_input(self, driver):
        input_field = driver.find_element_by_id(self.START_DATE)
        return input_field

    def get_day_input(self, driver):
        input_field = driver.find_element_by_id(self.NUMBER_ADD_DAYS)
        return input_field

    def enter(self, input_field, val, send_b):
        if(send_b):
            input_field.send_keys(Keys.BACKSPACE)
        input_field.send_keys(val)

    def advance_search(self, driver):
        links = driver.find_elements_by_tag_name('a')
        links[7].click()

    def new_search(self, driver):
        links = driver.find_elements_by_tag_name('a')
        links[6].click()

    def get_dropdown_offense(self, driver):
        selector = driver.find_element_by_name('rucrext')
        return selector

    def select_offense(self, selector, offense):
        selector.find_elements_by_tag_name('option')[offense].click()

    def submit(self, driver):
        driver.find_element_by_name(self.SUBMIT).click()

    def get_table_data(self, driver):
        time.sleep(1)
        tables = driver.find_elements_by_tag_name('table')
        for table in tables:
            table_rows = table.find_elements_by_tag_name('tr')
            for row in table_rows:
                t_data = row.find_elements_by_tag_name('td')
                next_td = False
                for td in t_data:
                    p_tags = td.find_elements_by_tag_name('p')
                    for p in p_tags:
                        # print(p.text)
                        if next_td:
                            self.location_set.append(p.text)
                            next_td = False
                        if p.text == self.offense_location:
                            next_td = True
        time.sleep(1)




# below is the api call I will eventually make to get all the locations
# https://maps.googleapis.com/maps/api/geocode/json?address=9524+HUNTER+LN,+AUSTIN,+78748&key=AIzaSyBEjHBaa-Pd--26vOhS1YzAlFH5KvDzTBg
