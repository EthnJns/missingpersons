
class OffenseDate:
    start_date = ''
    current_date = ''
    end_date = ''
    increment = 0
    month_mod = 12
    day_mod = {1:31, 2:28, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31}

    def __init__(self, start, end, inc):
        self.start_date = start
        self.current_date = start
        self.end_date = end
        self.increment = inc


    def increment_date(self):
        cd = self.current_date.split('/')
        month = int(cd[0])
        day = int(cd[1])
        year = int(cd[2])
        day = day + self.increment
        if(day > self.day_mod[month]):
            month = month + 1
            day = day % self.day_mod[month-1]
            if(month > 12):
                month = month % self.month_mod
                year = year + 1
        self.current_date = str(month)+'/'+str(day)+'/'+str(year)

    def passed_end_date(self):
        # print(self.current_date)
        cd = self.current_date.split('/')
        ed = self.end_date.split('/')
        current_month = int(cd[0])
        current_day = int(cd[1])
        current_year = int(cd[2])
        end_month = int(ed[0])
        end_day = int(ed[1])
        end_year = int(ed[2])
        if current_month >= end_month:
            if current_day >= end_day:
                if current_year >= end_year:
                    return True
        return False