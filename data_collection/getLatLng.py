import requests
import json
from types import SimpleNamespace

maps = 'https://maps.googleapis.com/maps/api/geocode/json?address='
key = '&key=AIzaSyBEjHBaa-Pd--26vOhS1YzAlFH5KvDzTBg'

def build_request(address):
    split_comma = address.split(',')
    phys_addr = split_comma[0].lstrip().rstrip().split(' ')
    city_zip = split_comma[1].lstrip().rstrip().split(' ')
    req = maps
    for add in phys_addr:
        req += add + '+'
    for pt in city_zip:
        req += pt + '+'
    req = req[:-1]+key
    
    return req
r = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address=9524+HUNTER+LN,+AUSTIN,+78748&key=AIzaSyBEjHBaa-Pd--26vOhS1YzAlFH5KvDzTBg')


data_file = open('data.txt', 'r')
data = data_file.readlines()
out = open('loc.txt', 'w+')
for datum in data:
    req = build_request(data[0])
    r = requests.get(build_request(datum))
    d = r.text
    data_object = json.loads(d, object_hook=lambda d: SimpleNamespace(**d))
    results = data_object.results
    lat_lng = str(results[0].geometry.location.lat)+':'+str(results[0].geometry.location.lng)
    out.write(lat_lng+',')

out.close()

